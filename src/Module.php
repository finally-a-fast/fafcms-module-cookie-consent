<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-cookie-consent/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-cookie-consent
 * @see https://www.finally-a-fast.com/packages/fafcms-cookie-consent/docs Documentation of fafcms-cookie-consent
 * @since File available since Release 1.0.0
 */

namespace fafcms\cookieconsent;

use fafcms\fafcms\inputs\Checkbox;
use fafcms\fafcms\inputs\ColorInput;
use fafcms\fafcms\inputs\RadioList;
use fafcms\fafcms\inputs\Textarea;
use fafcms\fafcms\inputs\TextInput;
use Yii;
use fafcms\helpers\abstractions\PluginModule;
use fafcms\helpers\classes\PluginSetting;
use fafcms\fafcms\components\FafcmsComponent;

/**
 * Class Module
 * @package fafcms\cookieconsent
 */
class Module extends PluginModule
{
    public function getPluginSettingRules(): array
    {
        return [
            [
                [
                    'active',
                    'palettePopupBackground',
                    'palettePopupText',
                    'paletteButtonBackground',
                    'paletteButtonText',
                    'theme',
                    'position',
                    'static',
                    'showLink',
                    'type',
                ],
                'required'
            ],
            [
                [
                    'palettePopupBackground',
                    'palettePopupText',
                    'paletteButtonBackground',
                    'paletteButtonText'
                ],
                'string',
                'max' => '7'
            ],
            //['theme', 'in', 'range' => ['block', 'classic', 'edgeless', 'wire']],
            //['position', 'in', 'range' => ['bottom', 'top', 'top-left', 'top-right', 'bottom-left', 'bottom-right']],
            [['active', 'static', 'showLink'], 'boolean'],
            ['contentMessage', 'string'],
            [
                [
                    'contentDismiss',
                    'contentDeny',
                    'contentAllow',
                    'contentLink',
                    'contentHref',
                ],
                'string',
                'max' => '255'
            ],
           // ['type', 'in', 'range' => ['info', 'opt-out', 'opt-in']],
        ];
    }

    protected function pluginSettingDefinitions(): array
    {
        return [
            new PluginSetting($this, [
                'name' => 'active',
                'label' => Yii::t('fafcms-cookie-consent', 'active'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'palettePopupBackground',
                'label' => Yii::t('fafcms-cookie-consent', 'palette_banner_background'),
                'defaultValue' => '#000000',
                'inputType' => ColorInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'palettePopupText',
                'label' => Yii::t('fafcms-cookie-consent', 'palette_banner_text'),
                'defaultValue' => '#ffffff',
                'inputType' => ColorInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'paletteButtonBackground',
                'label' => Yii::t('fafcms-cookie-consent', 'palette_button_background'),
                'defaultValue' => '#f1d600',
                'inputType' => ColorInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'paletteButtonText',
                'label' => Yii::t('fafcms-cookie-consent', 'palette_button_text'),
                'defaultValue' => '#000000',
                'inputType' => ColorInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'theme',
                'label' => Yii::t('fafcms-cookie-consent', 'theme'),
                'defaultValue' => 'block',
                'inputType' => RadioList::class,
                'items' => [
                    'block' => Yii::t('fafcms-cookie-consent', 'block'),
                    'classic' => Yii::t('fafcms-cookie-consent', 'classic'),
                    'edgeless' => Yii::t('fafcms-cookie-consent', 'edgeless'),
                    'wire' => Yii::t('fafcms-cookie-consent', 'wire'),
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'position',
                'label' => Yii::t('fafcms-cookie-consent', 'position'),
                'defaultValue' => 'bottom',
                'inputType' => RadioList::class,
                'items' => [
                    'bottom' => Yii::t('fafcms-cookie-consent', 'banner_bottom'),
                    'top' => Yii::t('fafcms-cookie-consent', 'banner_top'),
                    'top-left' => Yii::t('fafcms-cookie-consent', 'floating_top_left'),
                    'top-right' => Yii::t('fafcms-cookie-consent', 'floating_top_right'),
                    'bottom-left' => Yii::t('fafcms-cookie-consent', 'floating_bottom_left'),
                    'bottom-right' => Yii::t('fafcms-cookie-consent', 'floating_bottom_right'),
                ]
            ]),
            new PluginSetting($this, [
                'name' => 'static',
                'label' => Yii::t('fafcms-cookie-consent', 'static_position'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'showLink',
                'label' => Yii::t('fafcms-cookie-consent', 'show_policy_link'),
                'defaultValue' => false,
                'inputType' => Checkbox::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_BOOL,
            ]),
            new PluginSetting($this, [
                'name' => 'contentMessage',
                'label' => Yii::t('fafcms-cookie-consent', 'cookie_banner_message'),
                'inputType' => Textarea::class,
                'valueType' => FafcmsComponent::VALUE_TYPE_TEXT,
            ]),
            new PluginSetting($this, [
                'name' => 'contentDismiss',
                'label' => Yii::t('fafcms-cookie-consent', 'dismiss_button_text'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'contentDeny',
                'label' => Yii::t('fafcms-cookie-consent', 'deny_button_text'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'contentAllow',
                'label' => Yii::t('fafcms-cookie-consent', 'allow_button_text'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'contentLink',
                'label' => Yii::t('fafcms-cookie-consent', 'policy_link_text'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'contentHref',
                'label' => Yii::t('fafcms-cookie-consent', 'policy_link'),
                'inputType' => TextInput::class,
            ]),
            new PluginSetting($this, [
                'name' => 'type',
                'label' => Yii::t('fafcms-cookie-consent', 'compliance_type'),
                'defaultValue' => 'info',
                'inputType' => RadioList::class,
                'items' => [
                    'info' => Yii::t('fafcms-cookie-consent', 'info'),
                    'opt-out' => Yii::t('fafcms-cookie-consent', 'opt_out'),
                    'opt-in' => Yii::t('fafcms-cookie-consent', 'opt_in'),
                ]
            ]),
        ];
    }
}
