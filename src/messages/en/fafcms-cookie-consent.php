<?php

return [
    'active' => 'Active',
    'palette_banner_background' => 'Banner background color',
    'palette_banner_text' => 'Banner text color',
];
