<?php

return [
    'active' => 'Aktiv',
    'palette_banner_background' => 'Banner Hintergrundfarbe',
    'palette_banner_text' => 'Banner Textfarbe',
];
