<?php
/**
 * @author Christoph Möke <cm@finally-a-fast.com>
 * @copyright Copyright (c) 2019 Finally a fast
 * @license https://www.finally-a-fast.com/packages/fafcms-module-cookie-consent/license MIT
 * @link https://www.finally-a-fast.com/packages/fafcms-module-cookie-consent
 * @see https://www.finally-a-fast.com/packages/fafcms-module-cookie-consent/docs Documentation of fafcms-module-cookie-consent
 * @since File available since Release 1.0.0
 */

namespace fafcms\cookieconsent;

use Exception;
use yii\base\Application;
use fafcms\helpers\abstractions\PluginBootstrap;
use fafcms\helpers\abstractions\PluginModule;
use yii\base\BootstrapInterface;
use yii\i18n\PhpMessageSource;
use yiiui\yii2cookieconsent\assets\CookieConsentAsset;
use yiiui\yii2cookieconsent\widgets\CookieConsent;
use Yii;

/**
 * Class Bootstrap
 * @package fafcms\cookieconsent
 */
class Bootstrap extends PluginBootstrap implements BootstrapInterface
{
    public static $id = 'fafcms-cookie-consent';

    protected function bootstrapTranslations(Application $app, PluginModule $module): bool
    {
        if (!isset($app->i18n->translations['fafcms-cookie-consent'])) {
            $app->i18n->translations['fafcms-cookie-consent'] = [
                'class' => PhpMessageSource::class,
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true,
            ];
        }

        return true;
    }

    protected function bootstrapFrontendApp(Application $app, PluginModule $module): bool
    {
        try {
            if (!Yii::$app->request->isAjax && $module->getPluginSettingValue('active')) {
                Yii::$app->getView()->getAssetManager()->bundles[CookieConsentAsset::class] = [
                    'cssOptions' => [
                        'fafcmsAsync' => true,
                    ]
                ];

                CookieConsent::widget([
                    'palettePopupBackground' => $module->getPluginSettingValue('palettePopupBackground'),
                    'palettePopupText' => $module->getPluginSettingValue('palettePopupText'),
                    'paletteButtonBackground' => $module->getPluginSettingValue('paletteButtonBackground'),
                    'paletteButtonText' => $module->getPluginSettingValue('paletteButtonText'),
                    'theme' => $module->getPluginSettingValue('theme'),
                    'position' => $module->getPluginSettingValue('position'),
                    'static' => $module->getPluginSettingValue('static'),
                    'showLink' => $module->getPluginSettingValue('showLink'),
                    'contentMessage' => $module->getPluginSettingValue('contentMessage'),
                    'contentDismiss' => $module->getPluginSettingValue('contentDismiss'),
                    'contentDeny' => $module->getPluginSettingValue('contentDeny'),
                    'contentAllow' => $module->getPluginSettingValue('contentAllow'),
                    'contentLink' => $module->getPluginSettingValue('contentLink'),
                    'contentHref' => $module->getPluginSettingValue('contentHref'),
                    'type' => $module->getPluginSettingValue('type'),
                ]);
            }

            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
